import axios from 'axios';

export const GET_USERS = 'GET_USERS';

export function getUsers() {

    const request = axios.get('http://jsonplaceholder.typicode.com/users');

    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: GET_USERS, payload: data});
        });
    }

}