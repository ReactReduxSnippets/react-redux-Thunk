import {combineReducers} from 'redux';

import dispatchGetUsers from './dispatch_get_users';


const rootReducer = combineReducers({
    users: dispatchGetUsers
});

export default rootReducer;
