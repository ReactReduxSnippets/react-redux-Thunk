import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getUsers} from '../actions/index';

class App extends Component {

    componentDidMount() {
        this.props.getUsers();
    }

    renderUsers(user) {
        return (
            <li key={user.id} className="list-group-item">{user.name} - {user.email}</li>
        );
    }

    render() {
        return (
            <div>
                <h1>List of users</h1>
                <ul className="list-group">
                    {this.props.users.map(user => this.renderUsers(user))}
                </ul>
            </div>
        );
    }
}


function mapStateToProps({users}) {
    return {users};
}

export default connect(mapStateToProps, {getUsers})(App);